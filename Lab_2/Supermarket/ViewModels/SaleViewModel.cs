﻿using Supermarket.Models;
using Supermarket.Tools.Commands;
using Supermarket.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Supermarket.ViewModels
{
    public class SaleViewModel : ViewModelBase
    {
        public SaleViewModel() 
        {
            RemoveProductCommnand = new Command(RemoveProduct);
        }


        private ProductViewModel _selectedSaleProduct;
        public ProductViewModel SelectedSaleProduct
        {
            get
            {
                return _selectedSaleProduct;
            }
            set
            {
                _selectedSaleProduct = value;
                OnPropertyChanged(nameof(SelectedSaleProduct));
            }
        }
        public ICommand RemoveProductCommnand { get; set; }
        private void RemoveProduct(object args)
        {
            if (Products != null)
            {
                Products.Remove(SelectedSaleProduct);
                TotalAmount = 0;
                OnPropertyChanged(nameof(TotalAmount));
            }
        }


        #region DataProperties
        public DateTime SaleDate { get; set; }        
        public decimal TotalAmount
        {
            get
            {
                decimal totalAmout = decimal.Zero;
                foreach (var product in this.Products)
                {
                    totalAmout += product.Price * product.Count;
                }                
                return totalAmout;
            }  
            set
            {
                decimal totalAmout = decimal.Zero;
                foreach (var product in this.Products)
                {
                    totalAmout += product.Price;
                }
                OnPropertyChanged(nameof(TotalAmount));
            }
        }

        private PaymentMethod _paymentMethod;
        public PaymentMethod PaymentMethod
        {
            get
            {
                return _paymentMethod;
            }
            set
            {
                _paymentMethod = value;
                OnPropertyChanged(nameof(PaymentMethod));
            }
        }

        private ObservableCollection<ProductViewModel> _products = new ObservableCollection<ProductViewModel>();
        public ObservableCollection<ProductViewModel> Products 
        { 
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                OnPropertyChanged(nameof(Products));
                TotalAmount = 0;
                OnPropertyChanged(nameof(TotalAmount));
            }
        }
        #endregion
    }
}
