﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Spending : IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int SpendingId { get; set; }
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public DateTime SpendingDate { get; set; }

    public decimal Amount { get; set; }

    public int Price { get; set; }

    public string Description { get; set; } = null!;

    public int? EmployeeId { get; set; }

    //public int ProductId { get; set; }

    public int? SupplierId { get; set; }

    public virtual Employee? Employee { get; set; } = null!;

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();

    public virtual Supplier? Supplier { get; set; } = null!;

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
