﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SpendingDTOs.Request
{
    public class CreateEditSpendingRequest : IRequest
    {
        public int? Id { get; set; }
        public DateTime SpendingDate { get; set; }
        public decimal Amount { get; set; }
        public int Price { get; set; }
        public string Description { get; set; } = null!;
        public int? EmployeeId { get; set; }
        public int? SupplierId { get; set; }
    }
}
