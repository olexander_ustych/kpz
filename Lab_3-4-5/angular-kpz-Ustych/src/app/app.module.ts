import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ProductPreviewComponent } from './features/Components/product-preview/product-preview.component';
import { CurrencyUAPipe } from './shared/pipes/CurrencyUA/currency-ua.pipe';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from './core/interceptors/indexInterceptor';
import { LogInComponent } from './features/Components/log-in/log-in.component';
import { ProductDetailsComponent } from './features/Components/product-details/product-details.component';
import { CustomerViewComponent } from './features/Components/customer-view/customer-view.component';
import { ShowFullObjPipe } from './shared/pipes/ShowFullObj/show-full-obj.pipe';
import { LogOutComponent } from './features/Components/log-out/log-out.component';
import { HighlightDirective } from './shared/Directives/highlight/highlight.directive';

@NgModule({
  declarations: [
    AppComponent,
    ProductPreviewComponent,
    CurrencyUAPipe,
    LogInComponent,
    ProductDetailsComponent,
    CustomerViewComponent,
    ShowFullObjPipe,
    LogOutComponent,
    HighlightDirective    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
