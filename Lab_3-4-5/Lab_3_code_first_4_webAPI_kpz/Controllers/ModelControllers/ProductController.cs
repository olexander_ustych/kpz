﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.Controllers.GenericApiController;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.ModelControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : GenericApiController<Product, CreateEditProductRequest, ProductResponse>
    {
        public ProductController(IRepository<Product> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
