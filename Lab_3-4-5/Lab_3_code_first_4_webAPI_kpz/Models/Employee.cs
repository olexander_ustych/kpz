﻿using Lab_3_code_first_4_webAPI_kpz.Models.Classes;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Employee : User, IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int EmployeeId { get; set; }
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public new string FirstName { get; set; } = null!;

    public new string LastName { get; set; } = null!;

    public DateTime DateOfBirth { get; set; }

    public string Phone { get; set; } = null!;

    public string Position { get; set; } = null!;

    public double Salary { get; set; }

    public int? StoreId { get; set; }

    public virtual ICollection<Sale> Sales { get; set; } = new List<Sale>();

    public virtual ICollection<Spending> Spendings { get; set; } = new List<Spending>();

    public virtual StoreBranch? Store { get; set; } = null!;

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
