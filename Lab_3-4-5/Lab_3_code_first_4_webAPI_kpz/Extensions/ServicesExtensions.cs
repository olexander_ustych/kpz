﻿using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.Implementations;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;

namespace Lab_3_code_first_4_webAPI_kpz.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddCorsPolicy(this IServiceCollection services, string Policy)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(Policy, policy =>
                {
                    policy.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();                        
                });
            });
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Category>, CategoryRepository>();
            services.AddScoped<IRepository<Customer>, CustomerRepository>();
            services.AddScoped<IRepository<Discount>, DiscountRepository>();
            services.AddScoped<IRepository<Employee>, EmployeeRepository>();
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IRepository<Sale>, SaleRepository>();
            services.AddScoped<IRepository<Spending>, SpendingRepository>();
            services.AddScoped<IRepository<StoreBranch>, StoreBranchRepository>();
            services.AddScoped<IRepository<Supplier>, SupplierRepository>();
        }
    }
}
