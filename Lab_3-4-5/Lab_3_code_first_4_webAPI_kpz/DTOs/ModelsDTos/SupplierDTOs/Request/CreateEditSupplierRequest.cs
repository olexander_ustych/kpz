﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SupplierDTOs.Request
{
    public class CreateEditSupplierRequest: IRequest
    {
        public int? Id { get; set; }
        public string SupplierName { get; set; } = null!;
        public string ContactInfo { get; set; } = null!;
        public string Address { get; set; } = null!;
    }
}
