﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ustych.Olexander.RobotChallange2
{
    public static class DistanceHelper
    {
        public static int FindDistance(this Position a, Position b)
        {
            return (int)(((a.X - b.X) * (a.X - b.X)) + ((a.Y - b.Y) * (a.Y - b.Y)));
        }
    }
}
