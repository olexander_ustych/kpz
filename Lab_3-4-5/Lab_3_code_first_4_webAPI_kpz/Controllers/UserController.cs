﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.AuthOpt;
using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CustomerDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.EmployeeDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Models.Classes;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UstychChainSupermarkets5Context _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        private static bool _loggined = false;

        public UserController(UstychChainSupermarkets5Context context, IMapper mapper, IHttpContextAccessor httpContext) 
        {
            _context = context;
            _mapper = mapper;
            _httpContext = httpContext;
        }

        [HttpGet]
        public async Task<IResponse> Get()
        {
            /*await _httpContext.HttpContext.Session.LoadAsync();
            User user = new User()
            {
                FirstName = _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.Name),
                LastName = _httpContext.HttpContext.User.FindFirstValue(ClaimTypes.Surname)
            };
            await _httpContext.HttpContext.Session.CommitAsync();

            var a = _httpContext.HttpContext.Session.GetInt32("A");*/

            if(!_loggined)
            {
                return new CustomerResponse()
                {
                    Id = 0
                };
            }


            User user = new User()
            {
                FirstName = "Isai",
                LastName = "Carter"
            };

            User FullUser = null;

            if (user.FirstName != null && user.LastName != null)
            {
                var Customers = _context.Customers.ToList();
                var Employees = _context.Employees.ToList();

                foreach (var customer in Customers)
                {
                    if (customer.FirstName == user.FirstName && customer.LastName == user.LastName)
                    {
                        FullUser = customer;
                        break;
                    }
                }
                if (FullUser == null)
                {
                    foreach (var employee in Employees)
                    {
                        if (employee.FirstName == user.FirstName && employee.LastName == user.LastName)
                        {
                            FullUser = employee;
                            break;
                        }
                    }
                }

                if (FullUser is Customer)
                {
                    return _mapper.Map<CustomerResponse>(FullUser);
                }
                else if (FullUser is Employee)
                {
                    return _mapper.Map<EmployeeResponse>(FullUser);
                }
                else
                {
                    return new CustomerResponse()
                    {
                        Id = 0
                    };
                }
            }
            else
            {
                return new CustomerResponse()
                {
                    Id = 0
                };
            }
        }

        [HttpPost]
        public async Task<IResponse> Post(User user)
        {
            if(user == null)
            {
                return new CustomerResponse()
                {
                    Id = 0
                };
            }

            var Customers = _context.Customers.ToList();
            var Employees = _context.Employees.ToList();

            User? User = null;

            foreach( var customer in Customers)
            {
                if(customer.FirstName == user.FirstName && customer.LastName == user.LastName)
                {
                    User = customer;
                    break;
                }
            }
            if(User == null)
            {
                foreach (var employee in Employees)
                {
                    if (employee.FirstName == user.FirstName && employee.LastName == user.LastName)
                    {
                        User = employee;
                        break;
                    }
                }
            }

            if(User != null)
            {
                /*var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, user.FirstName),
                    new Claim(ClaimTypes.Surname, user.LastName )};*/

                //ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "MyCookiesAuthType");
                //await _httpContext.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));                


                /*_httpContext.HttpContext.Session.SetInt32("A", 15);
                await _httpContext.HttpContext.Session.CommitAsync();*/

                //var claims = new List<Claim> { new Claim(ClaimTypes.Name, username) };
                // создаем JWT-токен
                /*var jwt = new JwtSecurityToken(
                        issuer: AuthOptions.ISSUER,
                        audience: AuthOptions.AUDIENCE,
                        claims: claims,
                        expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(2)),
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));*/

                //return new JwtSecurityTokenHandler().WriteToken(jwt);*/
                _loggined = true;

                if (User is Customer)
                {
                    var res = _mapper.Map<CustomerResponse>(User);
                    //res.jwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                    return res;
                }
                else if(User is Employee)
                {
                    var res = _mapper.Map<EmployeeResponse>(User);
                    //res.jwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                    return res;
                }
                else
                {
                    return new CustomerResponse()
                    {
                        Id = 0
                    };
                }
            }
            else
            {
                return new CustomerResponse()
                {
                    Id = 0
                };
            }
        }

        [HttpHead]
        public async Task HeadLogOut()
        {
            _loggined = false;
            /*await _httpContext.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            await _httpContext.HttpContext.Session.CommitAsync();*/
        }
    }
}
