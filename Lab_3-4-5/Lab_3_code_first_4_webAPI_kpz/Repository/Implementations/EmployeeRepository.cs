﻿using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.Implementations
{
    public class EmployeeRepository: GenericRepository<UstychChainSupermarkets5Context, Employee>
    {
        public EmployeeRepository(UstychChainSupermarkets5Context dbContext): base(dbContext) { }
        protected override DbSet<Employee> DbSet => _dbContext.Employees;
    }
}
