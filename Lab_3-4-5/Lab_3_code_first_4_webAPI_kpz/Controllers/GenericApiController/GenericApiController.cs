﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Lab_3_code_first_4_webAPI_kpz.MyAutoMapper;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.GenericApiController
{
    [ApiController]
    public abstract class GenericApiController<TEntity, TRequest, TResponse> : ControllerBase
        where TEntity : class, IModel
        where TRequest: class, IRequest, new()
        where TResponse : class, IResponse, new()
    {
        private readonly IRepository<TEntity> repository;
        private readonly IMapper _mapper;

        protected GenericApiController(IRepository<TEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this._mapper = mapper;
        }

        /// <summary>
        /// Get all items
        /// </summary>        
        /// <returns>The list of items</returns>
        [HttpGet]
        public virtual ActionResult<IEnumerable<TResponse>> GetAll()
        {
            var entities = repository.GetAll().ToList();

            // тут ерор
            //var responses = _mapper.Map<TResponse>(entities);
            //var responses = _mapper.Map<TResponse>(entities as List<TEntity>);
            // тут null
            //var responses = _mapper.Map<TResponse>(entities as List<IModel>);

            // wtf? так працює, ахахахах
            var responses = new List<TResponse>();
            foreach (var entity in entities)
            {
                responses.Add(_mapper.Map<TResponse>(entity));
            }
            return Ok(responses);
        }

        /// <summary>
        /// Get specific item
        /// </summary>
        /// <param name="id">The ID of the item</param>
        /// <returns>The items</returns>
        [HttpGet("{id}")]
        public ActionResult<TResponse> GetOne(int id)
        {
            var foundEntity = repository.GetById(id);
            if (foundEntity == null)
            {
                return NotFound();
            }
            var foundEntityResponse = _mapper.Map<TResponse>(foundEntity);
            return Ok(foundEntityResponse);
        }

        /// <summary>
        /// Create item
        /// </summary>
        /// <param name="toCreate">The IRequest of the item</param>
        /// <returns>The IResponse</returns>
        [HttpPost]
        public ActionResult<TResponse> Create([FromBody] TRequest toCreate)
        {
            var creating = _mapper.Map<TEntity>(toCreate);
            var created = repository.Add(creating);
            var createdResponse = _mapper.Map<TResponse>(created);
            return Ok(createdResponse);
        }

        /// <summary>
        /// Update item
        /// </summary>
        /// <param name="id">The id of the item</param>
        /// <param name="toUpdate">The IRequest of the item to update</param>
        /// <returns>The IResponse</returns>
        [HttpPatch("{id}")]
        public ActionResult<TResponse> Update(int id, [FromBody] TRequest toUpdate)
        {
            var updating = _mapper.Map<TEntity>(toUpdate);
            updating.Id = id;
            var updated = repository.Update(updating);
            if (updated == null)
            {
                return NotFound();
            }
            var updatedResponse = _mapper.Map<TResponse>(updated);
            return Ok(updatedResponse);
        }

        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="id">The IRequest of the item to delete</param>
        /// <returns>The IResponse</returns>
        [HttpDelete("{id}")]
        public ActionResult<TResponse> Delete(int id)
        {
            var entity = repository.GetById(id);
            if (entity == null)       
            {
                return NotFound();
            }
            repository.Delete(entity);
            var entityResponse = _mapper.Map<TResponse>(entity);
            return Ok(entityResponse);
        }
    }
}
