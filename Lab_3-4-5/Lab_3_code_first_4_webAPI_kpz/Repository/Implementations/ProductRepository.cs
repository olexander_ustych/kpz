﻿using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.Implementations
{
    public class ProductRepository: GenericRepository<UstychChainSupermarkets5Context, Product>
    {
        public ProductRepository(UstychChainSupermarkets5Context dbContext) : base(dbContext) {}

        protected override DbSet<Product> DbSet => _dbContext.Products;
    }
}
