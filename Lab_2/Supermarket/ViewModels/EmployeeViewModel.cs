﻿using Supermarket.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Supermarket.ViewModels
{
    public class EmployeeViewModel : ViewModelBase
    {
        public EmployeeViewModel() { }

        #region DataProperties
        private string _firstName = null!;
        public string FirstName 
        { 
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                OnPropertyChanged(nameof(FirstName));
            }
        }

        private string _lastName = null!;
        public string LastName 
        {
            get 
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                OnPropertyChanged(nameof(LastName));
            }
        }

        private DateTime _birthDate;
        public DateTime DateOfBirth 
        { 
            get
            {
                return _birthDate;
            }
            set
            {
                _birthDate = value;
                OnPropertyChanged(nameof(DateOfBirth));
            }
        }

        private string _phone = null!;
        public string Phone 
        { 
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
                OnPropertyChanged(nameof(Phone));
            }
        }

        private string _position = null!;
        public string Position 
        { 
            get
            {
                return _position;
            }
            set
            {
                _position = value; 
                OnPropertyChanged(nameof(Position));
            }
        }

        private double _salary;
        public double Salary 
        { 
            get
            {
                return _salary;
            }
            set
            {
                _salary = value;
                OnPropertyChanged(nameof(Salary));
            }
        }
        #endregion
    }
}
