﻿using Lab_3_code_first_4_webAPI_kpz.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Lab_3_code_first_4_webAPI_kpz.Controllers.GenericApiController;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CategoryDTOs.Requests;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CategoryDTOs.Responses;
using AutoMapper;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.ModelControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : GenericApiController<Category, CreateEditCategoryRequest, CategoryResponse>
    {
        public CategoryController(IRepository<Category> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
