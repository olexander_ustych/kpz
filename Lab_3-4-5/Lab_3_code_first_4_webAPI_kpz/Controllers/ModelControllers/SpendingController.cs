﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.Controllers.GenericApiController;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SpendingDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SpendingDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.ModelControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpendingController : GenericApiController<Spending, CreateEditSpendingRequest, SpendingResponse>
    {
        public SpendingController(IRepository<Spending> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
