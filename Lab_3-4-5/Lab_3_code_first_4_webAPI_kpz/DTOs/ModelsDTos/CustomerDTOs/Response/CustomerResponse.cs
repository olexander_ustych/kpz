﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CustomerDTOs.Response
{
    public class CustomerResponse: IResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string ContactInfo { get; set; } = null!;
        public string Address { get; set; } = null!;
        public int? LoyaltyPoints { get; set; }
        public string? DestinationOfDelievery { get; set; }        
    }
}
