﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Request
{
    public class CreateEditStoreBranchRequest : IRequest
    {
        public int? Id { get; set; }
        public string Name { get; set; } = null!;
        public string Location { get; set; } = null!;
        public string Info { get; set; } = null!;
        public DateTime OpeningDate { get; set; }
    }
}
