import { Component, OnInit } from '@angular/core';
import { IUser } from './core/interfaces/iuser';
import { AuthService } from './core/services/auth-service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { //implements OnInit
  title = 'angular-kpz-Ustych';

  public user?: IUser;// = this.authService.isLogIned; 

  constructor(private authService: AuthService) {}

  /*ngOnInit(): void {
    
  }*/

  onLoggedIn(user: any) {
    console.log('In app module ' + user);
    this.user = user;
  }

  onDeactivate(data: any) {
    this.authService.getUser().subscribe({
      next: (data: any) => {  
        if(data.id != 0)
        {
          this.user = data;
        }       
        else{
          this.user = undefined;
        } 
      },
      error: (error: any) => {
        console.error(error);          
      } 
    });
  }

  onActivate(data:any) {
    this.authService.getUser().subscribe({
      next: (data: any) => {  
        if(data.id != 0)
        {
          this.user = data;
        }
        else{
          this.user = undefined;
        }         
      },
      error: (error: any) => {
        console.error(error);          
      } 
    });
  }
}
