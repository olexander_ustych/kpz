﻿using Supermarket.Models;
using Supermarket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Supermarket.Views
{
    /// <summary>
    /// Interaction logic for SaleUserControl.xaml
    /// </summary>
    public partial class SaleUserControl : UserControl
    {
        public SaleUserControl()
        {
            InitializeComponent();         
        }

        private void ComboBox_Initialized(object sender, EventArgs e)
        {
            (sender as ComboBox).ItemsSource = Enum.GetValues(typeof(PaymentMethod)).Cast<PaymentMethod>();
        }
    }
}
