import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef, 
    private renderer: Renderer2) {
    //this.el.nativeElement.style.backgroundColor = 'yellow';
 }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlightOn();
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlightOff();
  }

  private highlightOn() {
    this.renderer.setStyle(this.el.nativeElement, "font-weight", "bold");
  }
  
  private highlightOff() {
    this.renderer.setStyle(this.el.nativeElement, "font-weight", "normal");
  }
}
