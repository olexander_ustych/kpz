﻿using Supermarket.Tools.Commands;
using Supermarket.Models;
using Supermarket.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Supermarket.DependencyObjects;
using System.Windows;
using Supermarket.Tools.Mapping;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Supermarket.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        public DataViewModel() 
        {
            AddProductCommand = new Command(AddProduct);
            AddEmployeeCommand = new Command(AddEmployee);
            BuyCommand = new Command(Buy);            
            SelectCustomerCommand = new Command(SelectCustomer);
            SetVisibilityCommand = new Command(ControlVisibility);
            SelectEmployeeCommand = new Command(SelectEmployee);
        }

        /*#region DTO
        public (ObservableCollection<ProductViewModel> Products, CustomerViewModel SelectedCustomer) ToProductViewModel 
        { 
            get
            {
                return (Products, SelectedCustomer);
            }
        }
        #endregion*/

        #region Commands
        public ICommand AddProductCommand { get; set; }
        private void AddProduct(object args)
        {
            var newProduct = args as ProductInfo;
            if(newProduct is null) 
            {
                return;
            }
            if(newProduct.ProductName is null)
            {
                MessageBox.Show("ProductName can't be empty!", "Warning");
                return;
            }
            if(newProduct.Description is null)
            {
                MessageBox.Show("Description can't be empty!", "Warning");
                return;
            }
            if(newProduct.Count < 0)
            {
                MessageBox.Show("Count can't be less that zero!", "Warning");
                return;
            }
            if (newProduct.Price < 0)
            {
                MessageBox.Show("Price can't be less that zero!", "Warning");
                return;
            }
            /*if(newProduct.Category is null)
            {

            }*/

            var pr = MyAutoMapper.CreateMyMapper().Map<ProductViewModel>(newProduct);
            Products.Add(pr);
        }

        public ICommand AddEmployeeCommand { get; set; }
        private void AddEmployee(object args)
        {
            var newEmployee = args as EmployeeInfo;
            if(newEmployee is null) 
            {
                return;
            }

            if (newEmployee.FirstName is null)
            {
                MessageBox.Show("FirstName can't be empty!", "Warning");
                return;
            }
            else if (newEmployee.LastName is null)
            {
                MessageBox.Show("LastName can't be empty!", "Warning");
                return;
            }
            else if (newEmployee.Phone is null)
            {
                MessageBox.Show("Phone nuumber can't be empty!", "Warning");
                return;
            }            
            else if (newEmployee.Position is null)
            {
                MessageBox.Show("Position can't be empty!", "Warning");
                return;
            }
            else if (newEmployee.Salary == 0)
            {
                MessageBox.Show("Salary can't be 0!", "Warning");
                return;
            }

            if (newEmployee.Phone is not null)
            {
                Regex phoneRE = new Regex("^\\(?([0-9]{3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
                if (phoneRE.Match(newEmployee.Phone).Success == false)
                {
                    MessageBox.Show("Invalid Phone nuumber!", "Warning");
                    return;
                }
            }

            var emp = MyAutoMapper.CreateMyMapper().Map<EmployeeViewModel>(newEmployee);
            Employees.Add(emp);
        }



        private ProductViewModel _selectedProduct;
        public ProductViewModel SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }        
        public ICommand BuyCommand { get; set; }
        private void Buy(object args)
        {
            if (_selectedCustomer.Sale == null)
            {
                _selectedCustomer.Sale = new SaleViewModel()
                {
                    SaleDate = DateTime.Now,
                };
            }

            var pr = _selectedCustomer.Sale.Products.FirstOrDefault(p => p.ProductName == (args as ProductViewModel).ProductName);
            if (pr != null)
            {
                pr.Count++;
            }
            else
            {
                _selectedCustomer.Sale.Products.Add(new ProductViewModel()
                {
                    ProductName = (args as ProductViewModel).ProductName,
                    Description = (args as ProductViewModel).Description,
                    Price = (args as ProductViewModel).Price,
                    Category = (args as ProductViewModel).Category,
                    Count = 1
                });
            }

            OnPropertyChanged(nameof(SelectedCustomer));
            OnPropertyChanged(nameof(SelectedCustomer.Sale));
            SelectedCustomer.Sale.TotalAmount = 0;
            OnPropertyChanged(nameof(SelectedCustomer.Sale.TotalAmount));
        }

        private CustomerViewModel _selectedCustomer;
        public CustomerViewModel SelectedCustomer
        {
            get
            {
                return _selectedCustomer;
            }
            set
            {
                _selectedCustomer = value;
                OnPropertyChanged(nameof(SelectedCustomer));
            }
        }
        public ICommand SelectCustomerCommand { get; set; } = null!;
        private void SelectCustomer(object args)
        {
            VisibleControl = args.ToString();//"CutomerInfo";            
        }

        private string _visibleControl = "Buttons";
        public string VisibleControl
        {
            get
            {
                return _visibleControl;
            }
            set
            {
                _visibleControl = value;
                OnPropertyChanged(nameof(VisibleControl));
            }
        }
        public ICommand SetVisibilityCommand { get; set; } = null!;
        private void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }

        private EmployeeViewModel _selectedEmployee;
        public EmployeeViewModel SelectedEmployee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged(nameof(SelectedEmployee));
            }
        }
        public ICommand SelectEmployeeCommand { get; set; } = null!;
        private void SelectEmployee(object args)
        {
            VisibleControl = args.ToString();//"EmployeeInfo";            
        }
        #endregion


        #region Collections
        private ObservableCollection<CustomerViewModel> _customers = null!;
        public ObservableCollection<CustomerViewModel> Customers 
        { 
            get
            {
                return _customers;
            }
            set
            {
                _customers = value;
                OnPropertyChanged(nameof(Customers));
            }
        }

        private ObservableCollection<ProductViewModel> _products = null!;
        public ObservableCollection<ProductViewModel> Products 
        { 
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                OnPropertyChanged(nameof(Products));
            }
        }

        private ObservableCollection<SaleViewModel> _sales = null!;
        public ObservableCollection<SaleViewModel> Sales 
        { 
            get
            {
                return _sales;
            }
            set
            {
                _sales = value;
                OnPropertyChanged(nameof(Sales));
            }
        }

        private ObservableCollection<EmployeeViewModel> _employees = null!;
        public ObservableCollection<EmployeeViewModel> Employees
        {
            get
            {
                return _employees;
            }
            set
            {
                _employees = value;
                OnPropertyChanged(nameof(Employees));
            }
        }
        #endregion
    }
}
