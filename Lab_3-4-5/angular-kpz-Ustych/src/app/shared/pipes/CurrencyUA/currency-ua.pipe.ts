import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyUA'
})
export class CurrencyUAPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    value = value + "\u{20B4}";
    return value;
  }
}
