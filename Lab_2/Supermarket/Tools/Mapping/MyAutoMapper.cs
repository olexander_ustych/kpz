﻿using AutoMapper;
using Supermarket.DependencyObjects;
using Supermarket.Models;
using Supermarket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Tools.Mapping
{
    public class MyAutoMapper : Profile
    {
        public MyAutoMapper() 
        {
            AllowNullCollections = true;

            CreateMap<Customer, CustomerViewModel>().ReverseMap();
            CreateMap<Product, ProductViewModel>().ReverseMap();
            CreateMap<Sale, SaleViewModel>().ReverseMap();
            CreateMap<Employee, EmployeeViewModel>().ReverseMap();
            CreateMap<DataModel, DataViewModel>().ReverseMap();

            CreateMap<EmployeeViewModel, EmployeeInfo>().ReverseMap();
            CreateMap<ProductViewModel, ProductInfo>().ReverseMap();
        }
        public static IMapper CreateMyMapper()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MyAutoMapper>();
            }).CreateMapper();
        }
    }
}
