import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpProductService {

  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get('/api/Product').pipe(
      map((data: any) => {
        console.log("Successfully products receiving: ");
        console.log(data);
        return data;
      })
    );
  }

  getProduct(id: number) {
    return this.http.get(`/api/Product/${id}`).pipe(
      map((data: any) => {
        console.log("Successfully product " + id + " receiving: ");
        console.log(data);
        return data;
      })
    );
  }  


}
