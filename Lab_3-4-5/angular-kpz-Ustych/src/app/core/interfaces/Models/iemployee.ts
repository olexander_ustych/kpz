import { IUser } from "../iuser";

export interface IEmployee extends IUser {
    /*id: number,
    firstName: string, 
    lastName: string, */
    dateOfBirth: Date,
    phone: string, 
    position: string,
    salary: number, 
    storeId?: number 
}