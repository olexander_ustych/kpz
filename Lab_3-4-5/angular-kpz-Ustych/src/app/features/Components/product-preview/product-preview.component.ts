import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IProduct } from 'src/app/core/interfaces/Models/iproduct';
import { HttpProductService } from 'src/app/core/services/http-services/product-service/http-product-service.service';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent implements OnInit {
  public Products?: IProduct[]

  constructor(private httpService: HttpProductService,
    private router: Router) { }

  ngOnInit() {
    this.httpService.getProducts().subscribe(
      {
        next: (data: any) => {          
          this.Products = data;
          console.log("this.Products: ");
          console.log(this.Products);
        },
        error: (error: any) => {
          console.error(error);          
        }
      }
    );
  }

  Details(product: IProduct) {
    this.router.navigate(['/ProductDetails', product.id]);
  }
}
