export interface IProduct {
    id: number,
    productName: string, 
    description: string,    
    price: number, 
    count: number, 
    categoryId: number,
    spendingId?: number,
    supplierId?: number
}
