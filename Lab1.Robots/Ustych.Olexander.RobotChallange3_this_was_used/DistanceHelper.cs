﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Ustych.Olexander.RobotChallange3
{
    public static class DistanceHelper
    {
        public static int FindDistance(this Position a, Position b)
        {
            return (int)(((a.X - b.X) * (a.X - b.X)) + ((a.Y - b.Y) * (a.Y - b.Y)));
        }
    }
}
