import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpCustomerService {

  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get('/api/Customer').pipe(
      map((data: any) => {
        console.log("Successfully customer receiving: ");
        console.log(data);
        return data;
      })
    );
  }

  getCustomer(id: number) {
    return this.http.get(`/api/Customer/${id}`).pipe(
      map((data: any) => {
        console.log("Successfully customer " + id + " receiving: ");
        console.log(data);
        return data;
      })
    );
  }  
}
