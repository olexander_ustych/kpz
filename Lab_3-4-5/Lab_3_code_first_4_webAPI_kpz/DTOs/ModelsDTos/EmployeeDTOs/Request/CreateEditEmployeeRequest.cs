﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.EmployeeDTOs.Request
{
    public class CreateEditEmployeeRequest : IRequest
    {
        public int? Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public DateTime DateOfBirth { get; set; }
        public string Phone { get; set; } = null!;
        public string Position { get; set; } = null!;
        public double Salary { get; set; }
        public int? StoreId { get; set; }
    }
}
