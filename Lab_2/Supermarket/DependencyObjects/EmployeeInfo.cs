﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Supermarket.DependencyObjects
{
    public class EmployeeInfo : DependencyObject
    {
        public static readonly DependencyProperty FirstNameProperty =
            DependencyProperty.Register(nameof(FirstName), typeof(string),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public string FirstName 
        { 
            get
            {
                return (string)GetValue(FirstNameProperty);
            }
            set
            {
                SetValue(FirstNameProperty, value);
            }
        }

        public static readonly DependencyProperty LastNameProperty =
            DependencyProperty.Register(nameof(LastName), typeof(string),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public string LastName 
        { 
            get 
            {
                return (string)GetValue(LastNameProperty);
            } 
            set 
            {
                SetValue(LastNameProperty, value);
            } 
        }

        public static readonly DependencyProperty DateOfBirthProperty =
            DependencyProperty.Register(nameof(DateOfBirth), typeof(DateTime),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public DateTime DateOfBirth
        { 
            get
            {
                return (DateTime)GetValue(DateOfBirthProperty);
            } 
            set
            {                
                SetValue(DateOfBirthProperty, value);
            }
        }

        public static readonly DependencyProperty PhoneProperty =
            DependencyProperty.Register(nameof(Phone), typeof(string),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public string Phone 
        { 
            get
            {
                return ((string)GetValue(PhoneProperty));
            }
            set
            {
                SetValue(PhoneProperty, value);
            }
        }

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register(nameof(Position), typeof(string),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public string Position
        {
            get
            {
                return ((string)GetValue(PositionProperty));
            }
            set
            {
                SetValue(PositionProperty, value);
            }
        }

        public static readonly DependencyProperty SalaryProperty =
            DependencyProperty.Register(nameof(Salary), typeof(double),
                typeof(EmployeeInfo), new PropertyMetadata(null));
        public double Salary 
        { 
            get
            {
                return (double)GetValue(SalaryProperty);
            }
            set
            {
                SetValue(SalaryProperty, value);
            }
        }
    }
}
