﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Response
{
    public class StoreBranchResponse : IResponse
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Location { get; set; } = null!;
        public string Info { get; set; } = null!;
        public DateTime OpeningDate { get; set; }        
    }
}
