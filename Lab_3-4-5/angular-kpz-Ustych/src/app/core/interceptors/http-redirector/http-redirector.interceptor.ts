import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpRedirectorInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log("HttpRedirectorInterceptor");

    console.log("Old url" + request.url);
    /*if(request.url.includes("http://localhost:4200"))
    {
      request.url.replace('http://localhost:4200', 'https://localhost:7298')
    }*/
    //request.url = 'https://localhost:7298' + request.url;

    const httpsReq = request.clone({
      url: 'https://localhost:7298' + request.url
    });

    console.log("New url" + httpsReq.url);

    return next.handle(httpsReq);    
  }
}
