﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using System.Security.Principal;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IModel
    {
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> GetAll();
        TEntity? GetById(int id);
    }
}
