﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CategoryDTOs.Responses
{
    public class CategoryResponse: IResponse
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;        
    }
}
