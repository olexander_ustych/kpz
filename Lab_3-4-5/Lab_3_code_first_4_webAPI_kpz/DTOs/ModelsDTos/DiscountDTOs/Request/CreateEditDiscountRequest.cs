﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.DiscountDTOs.Request
{
    public class CreateEditDiscountRequest : IRequest
    {
        public int? Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProductId { get; set; }
    }
}
