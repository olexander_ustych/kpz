﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using Robot.Common;
using Ustych.Olexander.RobotChallange3;
using Assert = NUnit.Framework.Assert;


namespace UnitTests
{
    [TestClass]
    public class TestingDistanceHelper
    {
        [Test]
        [TestMethod]
        //[TestCase(1,1,5,5,24)]
        public void TestDistanceHelper()
        {
            var expected = 32;

            var first = new Position(1,1);
            var second = new Position(5,5);
            var distance = first.FindDistance(second);

            Assert.AreEqual(expected, distance);            
        }
    }
}
