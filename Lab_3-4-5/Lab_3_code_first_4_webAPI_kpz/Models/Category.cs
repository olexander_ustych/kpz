﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Category: IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int CategoryId { get; set; }    
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    //public int ProductId { get; set; }

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
