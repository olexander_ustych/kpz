import { TestBed } from '@angular/core/testing';

import { HttpRedirectorInterceptor } from './http-redirector.interceptor';

describe('HttpRedirectorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpRedirectorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HttpRedirectorInterceptor = TestBed.inject(HttpRedirectorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
