﻿using Lab_3_code_first_4_webAPI_kpz.Models.Classes;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Runtime.Loader;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Customer: User, IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int CustomerId { get; set; }    
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public new string FirstName { get; set; } = null!;

    public new string LastName { get; set; } = null!;

    public string ContactInfo { get; set; } = null!;

    public string Address { get; set; } = null!;

    public int? LoyaltyPoints { get; set; }

    public string? DestinationOfDelievery { get; set; }

    //public int? SaleId { get; set; }

    //public int? DiscountId { get; set; }

    public virtual Sale? Sale { get; set; }

    public virtual ICollection<Discount> Discounts { get; set; } = new List<Discount>();

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
