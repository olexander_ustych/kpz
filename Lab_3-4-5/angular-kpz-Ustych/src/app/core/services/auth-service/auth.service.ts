import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../classes/user';
import { IUser } from '../../interfaces/iuser';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isLogIned = false;
  public get isLogIned() {

    return this._isLogIned;
  }

  constructor(private http: HttpClient) { }

  LogIn(user: User) {
    return this.http.post<IUser>('/api/User', user).pipe(
      map((data: IUser) => {
        return data;
      })
    );
  }

  getUser() {
    return this.http.get('/api/User').pipe(
      map((data: any) => {
        return data;
      })
    );
  }

  LogOut() {
    return this.http.head('/api/User').pipe(
      map((data:any) => {
        return data;
      })
    );
  }
}
