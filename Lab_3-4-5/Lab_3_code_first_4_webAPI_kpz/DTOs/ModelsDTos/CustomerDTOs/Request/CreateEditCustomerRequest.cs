﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CustomerDTOs.Request
{
    public class CreateEditCustomerRequest: IRequest
    {
        public int? Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string ContactInfo { get; set; } = null!;
        public string Address { get; set; } = null!;
        public int? LoyaltyPoints { get; set; }
        public string? DestinationOfDelievery { get; set; }
    }
}
