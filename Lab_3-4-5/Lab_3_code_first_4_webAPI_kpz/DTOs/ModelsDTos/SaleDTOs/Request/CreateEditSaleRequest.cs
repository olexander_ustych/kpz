﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SaleDTOs.Request
{
    public class CreateEditSaleRequest : IRequest
    {
        public int? Id { get; set; }
        public DateTime SaleDate { get; set; }
        public decimal TotalAmount { get; set; }
        public string PaymentMethod { get; set; } = null!;
        public int? EmployeeId { get; set; }
        public int CustomerId { get; set; }
    }
}
