﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class StoreBranch: IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int StoreBranchId { get; set; }
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Location { get; set; } = null!;

    public string Info { get; set; } = null!;

    public DateTime OpeningDate { get; set; }

    //public int EmployeesId { get; set; }

    //public int ProductId { get; set; }

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
