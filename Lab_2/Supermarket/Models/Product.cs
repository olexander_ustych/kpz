﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Models
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public string ProductName { get; set; } = null!;
        [DataMember]
        public string Description { get; set; } = null!;
        private long count;
        [DataMember]
        public long Count
        {
            get
            {
                return count;
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidDataException();
                }
                count = value;
            }
        }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public Category Category { get;  set; }
    }
    public enum Category
    {
        Fruits_vegetables,
        Alcohol,
        Meat,
        Fish,
        Milk,
        Soft_drinks
    }
}
