﻿using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;

namespace Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Response
{
    public class ProductResponse : IResponse
    {
        public int Id { get; set; }
        public string ProductName { get; set; } = null!;
        public string Description { get; set; } = null!;
        public long Count { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public int? SpendingId { get; set; }
        public int? SupplierId { get; set; }        
    }
}
