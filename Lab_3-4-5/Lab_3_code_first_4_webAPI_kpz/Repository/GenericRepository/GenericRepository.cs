﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Security.Principal;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.GenericRepository
{
    public abstract class GenericRepository<TDbContext, TEntity> : IRepository<TEntity>
        where TDbContext : DbContext
        where TEntity : class, IModel
    {
        protected readonly TDbContext _dbContext;
        protected abstract DbSet<TEntity> DbSet { get; }

        protected GenericRepository(TDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
        public TEntity Add(TEntity entity)
        {
            DbSet.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public TEntity Update(TEntity entity)
        {
            DbSet.Update(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
            _dbContext.SaveChanges();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public virtual TEntity? GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }
    }
}
