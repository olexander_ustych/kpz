﻿using Supermarket.Models;
using Supermarket.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Supermarket.Tools.Commands;
using System.ComponentModel;
using System.Windows;

namespace Supermarket.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {
        #region DataProperties

        private string _firstName = null!;
        public string FirstName 
        { 
            get 
            {
                return _firstName;
            } 
            set 
            {
                _firstName = value;
                OnPropertyChanged(nameof(FirstName));
            } 
        }

        private string _lastName = null!;
        public string LastName 
        { 
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                OnPropertyChanged(nameof(LastName));
            }
        }

        private string _contactInfo = null!;
        public string ContactInfo 
        { 
            get
            {
                return _contactInfo;
            }
            set
            {
                _contactInfo = value;
                OnPropertyChanged(nameof(ContactInfo));
            }
        }

        private string _address = null!;
        public string Address 
        { 
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                OnPropertyChanged(nameof(Address));
            }
        }

        private SaleViewModel _sale = null!;
        public SaleViewModel Sale
        {
            get
            {
                return _sale;
            }
            set
            {
                _sale = value;
                OnPropertyChanged(nameof(Sale));
            }
        }
        #endregion
    }
}
