﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Models
{
    [DataContract]
    public class Sale
    {

        [DataMember]
        public DateTime SaleDate { get; set; }
        [DataMember]
        public decimal TotalAmount
        {
            get
            {
                decimal totalAmout = decimal.Zero;
                foreach (var product in this.Products)
                {
                    totalAmout += product.Price * product.Count;
                }
                return totalAmout;
            }
            set
            {

            }
        }
        [DataMember]
        public PaymentMethod PaymentMethod { get; set; } 
        [DataMember]
        public ICollection<Product> Products { get; set; } = new List<Product>();
        /*[DataMember]
        public Customer Customer { get; set; } = null!;*/
    }

    public enum PaymentMethod
    {
        Cash, 
        Card
    }
}
