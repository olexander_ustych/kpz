import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IProduct } from 'src/app/core/interfaces/Models/iproduct';
import { HttpProductService } from 'src/app/core/services/http-services/product-service/http-product-service.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  public product?: IProduct;

  private productId?: number;
  private routeSubscription: Subscription;

  constructor(private httpService: HttpProductService,
    private route: ActivatedRoute) {
      this.routeSubscription = route.params.subscribe(params=>this.productId=params['id']);
    }

  ngOnInit(): void {
    if(this.productId != undefined)
    {
      this.httpService.getProduct(this.productId).subscribe(
        {
          next: (data: any) => {          
            this.product = data;
            console.log("this.Products: ");
            console.log(this.product);
          },
          error: (error: any) => {
            console.error(error);          
          }
        }
      );
    }
  }
}