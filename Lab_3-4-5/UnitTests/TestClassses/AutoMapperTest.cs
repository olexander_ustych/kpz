﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Lab_3_code_first_4_webAPI_kpz.MyAutoMapper;
using Microsoft.CodeAnalysis.FlowAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.TestClassses
{
    [TestClass]
    public class AutoMapperTest
    {
        public IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MyMapper>();
        }).CreateMapper();

        [TestMethod]
        public void TestOnetoOne()
        {            
            var product = new Product()
            {
                Id = 0,
                CategoryId = 0,
                Price = 4
            };

            var productResponse = mapper.Map<ProductResponse>(product);

            Assert.IsTrue(productResponse.Id == product.Id);
        }

        [TestMethod]
        public void TestListMapping()
        {
            var products = new List<Product>()
            {
                new Product()
                {
                    Id = 0,
                    CategoryId = 0,
                    Price = 0
                },
                new Product()
                {
                    Id = 1,
                    CategoryId = 1,
                    Price = 1
                }
            };

            var productsResponse = mapper.Map<List<ProductResponse>>(products);

            Assert.IsTrue(products[1].Price == productsResponse[1].Price);
        }

        [TestMethod]
        public void TestListInterfacesMapping()
        {
            Assert.ThrowsException<AutoMapperMappingException>(TestAutoMapperException);
        }

        private void TestAutoMapperException()
        {
            var products = new List<IModel>()
            {
                new Product()
                {
                    Id = 0,
                    CategoryId = 0,
                    Price = 0
                },
                new Product()
                {
                    Id = 1,
                    CategoryId = 1,
                    Price = 1
                }
            };
            var productsResponse = mapper.Map<List<IResponse>>(products);
        }

        [TestMethod]
        public void TestListForLoopMapping()
        {
            var products = new List<IModel>()
            {
                new Product()
                {
                    Id = 0,
                    CategoryId = 0,
                    Price = 0
                },
                new Product()
                {
                    Id = 1,
                    CategoryId = 1,
                    Price = 1
                }
            };

            var productsResponse = new List<IResponse>();
            foreach (var product in products)
            {
                productsResponse.Add(mapper.Map<IResponse>(product));
            }

            Assert.IsTrue(products[1].Id == productsResponse[1].Id);
        }
    }
}
