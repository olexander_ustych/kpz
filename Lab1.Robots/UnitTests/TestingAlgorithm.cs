﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ustych.Olexander.RobotChallange3;

namespace UnitTests
{
    [TestClass]
    public class TestingAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position =  new Position(1,1),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() 
                {
                    Energy = 100,
                    Position = new Position(1, 5),
                    OwnerName = "Ustych Olexander"
                }
            };
            var command = algo.DoStep(robost, 0, map);
            Assert.IsTrue(command is MoveCommand);
        }
        [TestMethod]
        public void TestCollectCommand()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(1, 1),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 100,
                    Position = new Position(1, 1),
                    OwnerName = "Ustych Olexander"
                }
            };
            var command = algo.DoStep(robost, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }
        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(1, 1),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(1, 2),
                    OwnerName = "Ustych Olexander"
                }
            };
            var command = algo.DoStep(robost, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }
        [TestMethod]
        public void TestFindTheNearestStation()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 5),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(1, 2),
                    OwnerName = "Ustych Olexander"
                }
            };
            var StationPosition = algo.FindTheNearestStation(robost[0], map, robost);
            Assert.IsTrue(StationPosition == map.Stations[0].Position);
        }
        [TestMethod]
        public void TestIsAnyOfTheStationsFree()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 5),
                RecoveryRate = 1
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(8, 6),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(2, 5),
                    OwnerName = "Ustych Olexander"
                }
            };
            var Free = algo.IsAnyOfTheStationsFree(map.Stations.Select(s => s.Position).ToList(), robost);
            Assert.IsTrue(Free);
        }
        [TestMethod]
        public void TestIsStationFree()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 5),
                RecoveryRate = 1
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(8, 6),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(2, 5),
                    OwnerName = "Ustych Olexander"
                }
            };
            var Free = algo.IsStationFree(map.Stations[0].Position, robost[0], robost);
            Assert.IsTrue(Free);
        }
        [TestMethod]
        public void TestIsCloseToPosition()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 6),
                RecoveryRate = 1
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(8, 6),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(2, 5),
                    OwnerName = "Ustych Olexander"
                }
            };
            var True = algo.IsCloseToPosition(map.Stations[0].Position, robost[0].Position);
            Assert.IsTrue(True);
        }
        [TestMethod]
        public void TestGetPositionsFromRobotEnergy() 
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(2, 6),
                RecoveryRate = 1
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(8, 6),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 125,
                    Position = new Position(2, 5),
                    OwnerName = "Ustych Olexander"
                }
            };
            var list = algo.GetPositionsFromRobotEnergy(map, robost[0]);
            bool res = map.Stations.Select(s => s.Position).SequenceEqual(list);
            Assert.IsTrue(res);
        }
        [TestMethod]
        public void TestLongDistance()
        {
            var algo = new UstychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(0, 0),
                RecoveryRate = 1
            });
            var robost = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 100,
                    Position = new Position(99, 99),
                    OwnerName = "Ustych Olexander"
                }
            };
            var command = algo.DoStep(robost, 0, map);
            Assert.IsTrue(command is MoveCommand);
        }
    }
}
