﻿using Supermarket.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Supermarket.DependencyObjects
{
    public class ProductInfo : DependencyObject
    {
        public static readonly DependencyProperty ProductNameProperty =
            DependencyProperty.Register(nameof(ProductName), typeof(string),
                typeof(ProductInfo), new PropertyMetadata(null));
        public string ProductName
        {
            get
            {
                return (string)GetValue(ProductNameProperty);
            }
            set
            {
                SetValue(ProductNameProperty, value);
            }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register(nameof(Description), typeof(string),
                typeof(ProductInfo), new PropertyMetadata(null));
        public string Description
        {
            get
            {
                return (string)GetValue(DescriptionProperty);
            }
            set
            {
                SetValue(DescriptionProperty, value);
            }
        }

        public static readonly DependencyProperty CountProperty =
            DependencyProperty.Register(nameof(Count), typeof(long),
                typeof(ProductInfo), new PropertyMetadata(null));
        public long Count
        {
            get
            {
                return (long)GetValue(CountProperty);
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidDataException();
                }
                SetValue(CountProperty, value);
            }
        }

        public static readonly DependencyProperty PriceProperty =
            DependencyProperty.Register(nameof(Price), typeof(decimal),
                typeof(ProductInfo), new PropertyMetadata(null));
        public decimal Price
        {
            get
            {
                return (decimal)GetValue(PriceProperty);
            }
            set
            {
                SetValue(PriceProperty, value);
            }
        }

        public static readonly DependencyProperty CategoryProperty =
            DependencyProperty.Register(nameof(Category), typeof(Category),
                typeof(ProductInfo), new PropertyMetadata(null));
        public Category Category
        {
            get
            {
                return ((Category)GetValue(CategoryProperty));
            }
            set
            {
                SetValue(CategoryProperty, value);
            }
        }
    }
}
