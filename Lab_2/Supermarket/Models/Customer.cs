﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Models
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public string FirstName { get; set; } = null!;
        [DataMember]
        public string LastName { get; set; } = null!;
        [DataMember]
        public string ContactInfo { get; set; } = null!;
        [DataMember]
        public string Address { get; set; } = null!;
        [DataMember]
        public Sale Sale { get; set; } = new Sale();
    }
}
