import { IUser } from "../iuser"

export interface ICustomer extends IUser {
    /*id: number,
    firstName: string, 
    lastName: string, */
    contactInfo: string
    address: string
    loyaltyPoints?: number,
    destinationOfDelievery?: string
}