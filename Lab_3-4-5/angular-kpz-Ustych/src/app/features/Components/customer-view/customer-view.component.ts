import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ICustomer } from 'src/app/core/interfaces/Models/icutomer';
import { HttpCustomerService } from 'src/app/core/services/http-services/customer-service/http-customer.service';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss']
})
export class CustomerViewComponent implements OnInit {
  public customer?: ICustomer;

  private cutomerId?: number;
  private routeSubscription: Subscription;

  constructor(private httpService: HttpCustomerService,
    private route: ActivatedRoute) {
      this.routeSubscription = route.params.subscribe(params=>this.cutomerId=params['id']);
    }

  ngOnInit(): void {
    if(this.cutomerId != undefined)
    {
      this.httpService.getCustomer(this.cutomerId).subscribe(
        {
          next: (data: any) => {          
            this.customer = data;
            console.log("this.Cutomer: ");
            console.log(this.customer);
          },
          error: (error: any) => {
            console.error(error);          
          }
        }
      );
    }
  }
}

