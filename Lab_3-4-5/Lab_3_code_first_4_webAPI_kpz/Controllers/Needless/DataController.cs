﻿using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Lab_3_code_first_4_webAPI_kpz.Models.ViewModels;
using Lab_3_code_first_4_webAPI_kpz.Repository.Implementations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SQLitePCL;
using System.Text.Json;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.Needless
{
    [ApiController]
    [Route("api/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly UstychChainSupermarkets5Context _context;
        private readonly ProductRepository _productRepository;

        public DataController(UstychChainSupermarkets5Context context)
        {
            _context = context;
            _productRepository = new ProductRepository(_context);
        }

        [HttpGet]
        public IEnumerable<Product> GetData()
        {
            return _productRepository.GetAll().ToList();
            //return _context.Products.ToList();

            // param: (string type)
            /*object data = null;
            switch (type)
            {
                case "Product":
                    {
                        data = new DataViewModel<Product>();
                    }
                    break;
                default:
                    break;
            }*/


            /*var productsToSend = _context.Products.ToList();
            return  productsToSend.Select(p =>
            {
                var res = new Product()
                {
                    ProductName = p.ProductName,
                    Description = p.Description,
                    Price = p.Price
                };
                return res;
            }).ToList().GetRange(0, 20);*/
        }
    }
}
