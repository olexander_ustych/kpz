﻿using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.Implementations
{
    public class SupplierRepository : GenericRepository<UstychChainSupermarkets5Context, Supplier>
    {
        public SupplierRepository(UstychChainSupermarkets5Context dbContext) : base(dbContext) { }
        protected override DbSet<Supplier> DbSet => _dbContext.Suppliers;
    }
}
