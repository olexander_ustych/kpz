﻿using Microsoft.VisualBasic;
using Supermarket.Tools.DataSerializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Supermarket.Models
{
    public class DataModel
    {        
        public IEnumerable<Customer> Customers { get; set; } = null!;
        public IEnumerable<Product> Products { get; set; } = null!;
        public IEnumerable<Sale> Sales { get; set; } = null!;      
        public IEnumerable<Employee> Employees { get; set; } = null!;


        private static string DataPath = "data.dat"; 
        public static DataModel Load()
        {
            if(File.Exists(DataPath))
            {
                return DataSerializer.DeserializeData(DataPath);
            }
            return new DataModel();
        }
        public void Save() 
        {
            DataSerializer.SerializeData(DataPath, this);
        }
    }
}
