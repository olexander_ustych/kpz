﻿using Supermarket.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Supermarket.Tools.Converter
{
    public class CategoryImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var category = (int)((Category)value);
            //C:\Users\FireM\source\KPZ\kpz\Lab_2\Supermarket\Images\Category_0.png
            var uri = new Uri(string.Format(@"C:\Users\FireM\source\KPZ\kpz\Lab_2\Supermarket\Images\Categories\Category_{0}.png", category), UriKind.Absolute);
            //C:\Users\FireM\source\KPZ\kpz\Lab_2\Supermarket\Tools\Converter\CategoryImageConverter.cs
            //var uri = new Uri(string.Format(@"../../Images/Category_{0}.png", category), UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
