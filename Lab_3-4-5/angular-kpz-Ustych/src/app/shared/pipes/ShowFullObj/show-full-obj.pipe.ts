import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showFullObj'
})
export class ShowFullObjPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const obj = value as any;
    value = "";
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        if(key == 'price')
        {
          const element = obj[key];        
          value += `<div>
                      <label>${key}: </label>
                      <label>${element} | currencyUA </label>
                    </div>`;
        }
        else if(key != 'id')
        {
          const element = obj[key];        
          if(element != null)
          {
            value += `<div>
                        <label>${key}: </label>
                        <label>${element}</label>
                      </div>`;
          }
        }
      }
    }
    return value;   
  }
}

/*
<div>
  <label>Count: </label>
  {{ product?.count }}
</div>
*/