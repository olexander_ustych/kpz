﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Product: IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int ProductId { get; set; }    
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public string ProductName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public long Count { get; set; }

    public decimal Price { get; set; }

    public int CategoryId { get; set; }

    public int? SpendingId { get; set; }

    //public int? StoreId { get; set; }

    public int? SupplierId { get; set; }

    public virtual Category Category { get; set; } = null!;

    public virtual ICollection<Discount> Discounts { get; set; } = new List<Discount>();

    public virtual Spending? Spending { get; set; } = null!;

    //public virtual StoreBranch? Store { get; set; } = null!;
    public virtual ICollection<StoreBranch> StoreBranches { get; set; } = new List<StoreBranch>();

    public virtual Supplier? Supplier { get; set; } = null!;

    public virtual ICollection<Sale> Sales { get; set; } = new List<Sale>();

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
