﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.Controllers.GenericApiController;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab_3_code_first_4_webAPI_kpz.Controllers.ModelControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreBranchController : GenericApiController<StoreBranch, CreateEditStoreBranchRequest, StoreBranchResponse>
    {
        public StoreBranchController(IRepository<StoreBranch> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
