import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductPreviewComponent } from './features/Components/product-preview/product-preview.component';
import { LogInComponent } from './features/Components/log-in/log-in.component';
import { ProductDetailsComponent } from './features/Components/product-details/product-details.component';
import { CustomerViewComponent } from './features/Components/customer-view/customer-view.component';
import { LogOutComponent } from './features/Components/log-out/log-out.component';

const routes: Routes = [
  {path: '', component: ProductPreviewComponent},
  {path: 'logging', component: LogInComponent},
  {path: 'ProductDetails/:id', component: ProductDetailsComponent},
  {path: 'CustomerView/:id', component: CustomerViewComponent},
  {path: 'logout', component: LogOutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
