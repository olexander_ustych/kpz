﻿using AutoMapper;
using Supermarket.Models;
using Supermarket.Tools.Mapping;
using Supermarket.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace Supermarket
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataModel _dataModel = null!;
        private DataViewModel _dataViewModel = null!;
        private readonly IMapper mapper = MyAutoMapper.CreateMyMapper();

        /*public App()
        {
            _dataModel = DataModel.Load();
            //MainWindow.DataContext = _dataModel;
            var window = new MainWindow()
            {
                DataContext = _dataModel
            };
            MainWindow.Show();
        }*/

        protected override void OnStartup(StartupEventArgs e)
        {
            _dataModel = DataModel.Load();
            _dataViewModel = mapper.Map<DataViewModel>(_dataModel);

            base.OnStartup(e);
        }

        protected override void OnActivated(EventArgs e)
        {
            if(MainWindow != null)
            {
                if(MainWindow.DataContext == null)
                {
                    /*_dataViewModel.Employees = new System.Collections.ObjectModel.ObservableCollection<EmployeeViewModel>
                    {
                        new EmployeeViewModel()
                        {                            
                            FirstName = "Helmer",
                            LastName = "Labadie",
                            DateOfBirth = new DateTime(2000, 04, 12, 0, 6, 49),
                            Phone = "787-551-3714",
                            Position = "Worker",
                            Salary = 73477,
                        },
                        new EmployeeViewModel()
                        {
                            FirstName = "Kimberly",
                            LastName = "Kulas",
                            DateOfBirth = new DateTime(2000, 04, 21, 0, 6, 49),
                            Phone = "947-482-4954",
                            Position = "Cashier",
                            Salary = 27289,
                        },
                        new EmployeeViewModel()
                        {
                            FirstName = "Francisca",
                            LastName = "Nader",
                            DateOfBirth = new DateTime(2000, 02, 21, 0, 6, 49),
                            Phone = "383-326-5082",
                            Position = "Cashier",
                            Salary = 36589,
                        },
                    };*/
                    MainWindow.DataContext = _dataViewModel;                    
                }
            }

            base.OnActivated(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                _dataModel = mapper.Map<DataModel>(_dataViewModel);
                _dataModel.Save();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error while exiting: " + exception.Message);
                throw;
            }

            base.OnExit(e);
        }
    }
}
