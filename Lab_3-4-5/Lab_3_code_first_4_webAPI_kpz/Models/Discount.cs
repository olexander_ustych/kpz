﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Lab_3_code_first_4_webAPI_kpz.Models;

public class Discount: IModel
{
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int DiscountId { get; set; }
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public int ProductId { get; set; }

    public virtual Product Product { get; set; } = null!;

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();

    public override string ToString()
    {
        Type type = this.GetType();
        string res = "";
        foreach (PropertyInfo propertyInfo in type.GetProperties())
        {
            if (propertyInfo.CanRead)
            {
                res += propertyInfo.Name + ": ";
                res += propertyInfo.GetValue(this, null) + ", ";
            }
        }
        res += "\n";
        return res;
    }
}
