import { Component, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/core/classes/user';
import { AuthService } from 'src/app/core/services/auth-service/auth.service';
import { IUser } from '../../../core/interfaces/iuser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent {
  public display = false;
  public user: User = new User("", "");

  constructor(private authService: AuthService,
    private router: Router) { }

  //onLoggedIn
  @Output() onLoggedIn = new EventEmitter<IUser>();
  LoggedIn(user: IUser) {
      this.onLoggedIn.emit(user);
  }

  LogIn() {
    this.authService.LogIn(this.user).subscribe(
      {
        next: (data: IUser) => {
          if(data.id == 0) {
            this.display = true;
          }
          else {
            this.display = false;
            this.LoggedIn(data);
            this.router.navigate(['/CustomerView', data.id]);
          }
        },
        error: (error: any) => {
          console.error(error);          
        }
      }
    );
  }
}
