﻿using Lab_3_code_first_4_webAPI_kpz.Data;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Repository.GenericRepository;
using Microsoft.EntityFrameworkCore;

namespace Lab_3_code_first_4_webAPI_kpz.Repository.Implementations
{
    public class CustomerRepository: GenericRepository<UstychChainSupermarkets5Context, Customer>
    {
        public CustomerRepository(UstychChainSupermarkets5Context _dbContext) : base(_dbContext) { }
        protected override DbSet<Customer> DbSet => _dbContext.Customers;
    }
}
