﻿using Supermarket.Models;
using Supermarket.Tools.Commands;
using Supermarket.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Supermarket.ViewModels
{
    public class ProductViewModel : ViewModelBase
    {
        #region Dataproperties
        private string _productName = null!;
        public string ProductName 
        {
            get
            {
                return _productName;
            }
            set
            {
                _productName = value;
                OnPropertyChanged(nameof(ProductName));
            }
        }

        private string _description = null!;
        public string Description 
        { 
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        private long _count;        
        public long Count
        {
            get
            {
                return _count;
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidDataException();
                }
                _count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        private decimal _price;
        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                OnPropertyChanged(nameof(Price));
            }
        }

        public Category Category { get; set; }
        #endregion
    }
}
