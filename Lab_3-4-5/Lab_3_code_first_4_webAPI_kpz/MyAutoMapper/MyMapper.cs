﻿using AutoMapper;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.IDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CategoryDTOs.Requests;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CategoryDTOs.Responses;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CustomerDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.CustomerDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.DiscountDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.DiscountDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.EmployeeDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.EmployeeDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.ProductDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SaleDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SaleDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SpendingDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SpendingDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.StoreBranchDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SupplierDTOs.Request;
using Lab_3_code_first_4_webAPI_kpz.DTOs.ModelsDTos.SupplierDTOs.Response;
using Lab_3_code_first_4_webAPI_kpz.Models;
using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;

namespace Lab_3_code_first_4_webAPI_kpz.MyAutoMapper
{
    public class MyMapper : Profile
    {
        public MyMapper()
        {
            AllowNullCollections = true;

            CreateMap<IModel, IRequest>().ReverseMap();
            CreateMap<IModel, IResponse>().ReverseMap();

            /*CreateMap<IEnumerable<IModel>, IEnumerable<IRequest>>().ReverseMap();
            CreateMap<IEnumerable<IModel>, IEnumerable<IResponse>>().ReverseMap()*/;



            CreateMap<Category, CreateEditCategoryRequest>().ReverseMap();
            CreateMap<Category, CategoryResponse>().ReverseMap();

            CreateMap<Customer, CreateEditCustomerRequest>().ReverseMap();
            CreateMap<Customer, CustomerResponse>().ReverseMap();

            CreateMap<Discount, CreateEditDiscountRequest>().ReverseMap();
            CreateMap<Discount, DiscountResponse>().ReverseMap();

            CreateMap<Employee, CreateEditEmployeeRequest>().ReverseMap();
            CreateMap<Employee, EmployeeResponse>().ReverseMap();



            CreateMap<Product, CreateEditProductRequest>().ReverseMap();
            CreateMap<Product, ProductResponse>().ReverseMap();

            /*CreateMap<IEnumerable<Product>, IEnumerable<CreateEditProductRequest>>().ReverseMap();
            CreateMap<IEnumerable<Product>, IEnumerable<ProductResponse>>().ReverseMap();*/



            CreateMap<Sale, CreateEditSaleRequest>().ReverseMap();
            CreateMap<Sale, SaleResponse>().ReverseMap();

            CreateMap<Spending, CreateEditSpendingRequest>().ReverseMap();
            CreateMap<Spending, SpendingResponse>().ReverseMap();

            CreateMap<StoreBranch, CreateEditStoreBranchRequest>().ReverseMap();
            CreateMap<StoreBranch, StoreBranchResponse>().ReverseMap();

            CreateMap<Supplier, CreateEditSupplierRequest>().ReverseMap();
            CreateMap<Supplier, SupplierResponse>().ReverseMap();
        }
    }
}
