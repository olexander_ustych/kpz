﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Robot.Common;

namespace Ustych.Olexander.RobotChallange3
{
    public class UstychAlgorithm : IRobotAlgorithm
    {
        public string Author => "Ustych Olexander";

        public int Round { get; set; }

        static int counter = 0;

        public UstychAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
            counter++;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            Round++;
        }

        private static EnergyStation MainStation = null;


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot robot = robots[robotToMoveIndex];

            if (MainStation != null)
            {
                if (MainStation.Position.FindDistance(robot.Position) < 10)
                {
                    return new MoveCommand()
                    {
                        NewPosition = MainStation.Position
                    };
                }            
                else
                {
                    int min = int.MaxValue;
                    Position minPos = new Position();
                    foreach(EnergyStation station in map.Stations)
                    {
                        if(MainStation.Position.FindDistance(station.Position) < min)
                        {
                            min = MainStation.Position.FindDistance(station.Position);
                            minPos = MainStation.Position;
                        }
                    }

                    int counterNearStation = 0;
                    foreach (var someRobot in robots)
                    {
                        if(someRobot.OwnerName == this.Author)
                        {
                            if(someRobot.Position.FindDistance(MainStation.Position) < 2)
                            {
                                counterNearStation++;
                            }
                        }
                    }

                    if(counterNearStation > 9)
                    {
                        return new MoveCommand()
                        {
                            NewPosition = MainStation.Position
                        };
                    }
                }
            }
            
            if (robot.Energy >= 125 && robots.Where((Robot.Common.Robot x) => x.OwnerName.Contains(this.Author)).Count() < 100 && IsAnyOfTheStationsFree(GetPositionsFromRobotEnergy(map, robot), robots))
            {
                return new CreateNewRobotCommand
                {
                    NewRobotEnergy = 50
                };
            }

            Position position = FindTheNearestStation(robots[robotToMoveIndex], map, robots);
            if (position == null)
            {
                return null;
            }
            else
            {
                if(Round <= 3 && MainStation != null)
                {
                    MainStation = new EnergyStation();
                    MainStation.Position = position;
                }
            }

            if (position == robot.Position || IsCloseToPosition(position, robot.Position))
            {
                return new CollectEnergyCommand();
            }

            if (new Random().Next(3, 10) % 10 == 0)
            {
                MoveCommand moveCommand = GenerateMoveCommand(robot, map);
                if (moveCommand != null && moveCommand.NewPosition.FindDistance(robot.Position) < robot.Energy / 2)
                {
                    return moveCommand;
                }
            }

            if (position.FindDistance(robot.Position) > robot.Energy)
            {
                return new MoveCommand
                {
                    NewPosition = new Position(position.X / 5, position.Y / 5) 
                };
            }            

            return new MoveCommand
            {
                NewPosition = position
            };
        }

        public MoveCommand GenerateMoveCommand(Robot.Common.Robot robot, Map map)
        {
            int num = map.Stations.Where((EnergyStation x) => x.Position.X < map.MaxPozition.X / 2 && x.Position.Y < map.MaxPozition.Y / 2).Count();
            int num2 = map.Stations.Where((EnergyStation x) => x.Position.X > map.MaxPozition.X / 2 && x.Position.Y < map.MaxPozition.Y / 2).Count();
            int num3 = map.Stations.Where((EnergyStation x) => x.Position.X > map.MaxPozition.X / 2 && x.Position.Y > map.MaxPozition.Y / 2).Count();
            int num4 = map.Stations.Where((EnergyStation x) => x.Position.X < map.MaxPozition.X / 2 && x.Position.Y > map.MaxPozition.Y / 2).Count();
            if (num >= num2 && num >= num3 && num >= num4)
            {
                if (robot.Position.X < map.MaxPozition.X / 2 && robot.Position.Y < map.MaxPozition.Y / 2)
                {
                    return null;
                }

                return new MoveCommand
                {
                    NewPosition = new Position(robot.Position.X - 5, robot.Position.Y - 5)
                };
            }

            if (num2 >= num && num2 >= num3 && num2 >= num4)
            {
                if (robot.Position.X > map.MaxPozition.X / 2 && robot.Position.Y < map.MaxPozition.Y / 2)
                {
                    return null;
                }

                return new MoveCommand
                {
                    NewPosition = new Position(robot.Position.X + 5, robot.Position.Y - 5)
                };
            }

            if (num3 >= num && num3 >= num2 && num3 >= num4)
            {
                if (robot.Position.X > map.MaxPozition.X / 2 && robot.Position.Y > map.MaxPozition.Y / 2)
                {
                    return null;
                }

                return new MoveCommand
                {
                    NewPosition = new Position(robot.Position.X + 5, robot.Position.Y + 5)
                };
            }

            if (robot.Position.X < map.MaxPozition.X / 2 && robot.Position.Y > map.MaxPozition.Y / 2)
            {
                return null;
            }

            return new MoveCommand
            {
                NewPosition = new Position(robot.Position.X - 5, robot.Position.Y + 5)
            };
        }

        public Position FindTheNearestStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            Position position = null;
            int num = int.MaxValue;
            foreach (EnergyStation station in map.Stations)
            {
                if (IsStationFree(station.Position, movingRobot, robots))
                {
                    int num2 = station.Position.FindDistance(movingRobot.Position);
                    if (num2 < num)
                    {
                        num = num2;
                        position = station.Position;
                    }
                }
            }

            return (position == null) ? null : position;
        }

        public bool IsAnyOfTheStationsFree(List<Position> stations, IList<Robot.Common.Robot> robots)
        {
            if (stations.Count == 0)
            {
                return false;
            }

            int num = 0;
            foreach (Robot.Common.Robot robot in robots)
            {
                if (stations.Contains(robot.Position))
                {
                    num++;
                }
            }

            return num != stations.Count;
        }

        public bool IsStationFree(Position station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station, movingRobot, robots);
        }

        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (Robot.Common.Robot robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsCloseToPosition(Position station, Position robot)
        {
            return station.FindDistance(robot) == 1;
        }

        public List<Position> GetPositionsFromRobotEnergy(Map map, Robot.Common.Robot myRobot)
        {
            List<Position> list = new List<Position>();
            Position b = new Position
            {
                X = myRobot.Position.X,
                Y = myRobot.Position.Y
            };
            foreach (EnergyStation station in map.Stations)
            {
                if (station.Position.FindDistance(b) <= 100)
                {
                    list.Add(station.Position);
                }
            }

            return list;
        }
    }
}
