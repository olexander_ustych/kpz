﻿using Lab_3_code_first_4_webAPI_kpz.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Lab_3_code_first_4_webAPI_kpz.Models.ViewModels
{
    public class DataViewModel<T> 
        where T : class, IModel
    {
        public DbSet<T>? Data { get; set; }
    }
}
