﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Supermarket.Tools.Commands
{
    public class Command : ICommand
    {
        public Command(Action<object> action, Predicate<object> predicate = null)
        {
            ExecuteDelegate = action;
            CanExecuteDelegate = predicate;
        }

        public Action<object> ExecuteDelegate;
        public Predicate<object> CanExecuteDelegate;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object? parameter)
        {
            return CanExecuteDelegate == null || CanExecuteDelegate(parameter);
        }

        public void Execute(object? parameter)
        {
            if(CanExecute(parameter))
            {
                ExecuteDelegate(parameter);
            }
        }
    }
}
